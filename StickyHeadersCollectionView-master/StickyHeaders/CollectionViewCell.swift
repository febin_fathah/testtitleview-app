//
//  CollectionViewCell.swift
//  StickyHeaders
//
//  Created by Febin Fathah on 16/11/17.
//  Copyright © 2017 Cocoacasts. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var container: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        container?.layer.borderColor = UIColor.lightGray.cgColor
        container?.layer.borderWidth = 0.5
    }

}
