//
//  ViewController.swift
//  TestTitleView App
//
//  Created by XtraMac1 on 19/10/17.
//  Copyright © 2017 XtraMac1. All rights reserved.
//

import UIKit


enum ScrollDirection {
    case up,down
}

class ViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var headerView: UIView!
    var numberOfSections: Int? = 0
    var numberOfRowsInSection: Int? = 0
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBarContainerView: UIView!
    
    private var images: [UIImage]? = [
        UIImage(named: "1")!,
        UIImage(named: "2")!,
        UIImage(named: "3")!,
        UIImage(named: "0")!
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        numberOfRowsInSection = 20
        numberOfSections = 1
        pageControl.numberOfPages = (images?.count)!
        setupHeader()
        tableView.reloadData()
}
    
    // MARK:- Functions
    func setupHeader() {
        let parallaxHeight: CGFloat = tableView.frame.height * 0.75
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.white
        //adding view as parallax header to table view is straightforward
        tableView.parallaxHeader.view = headerView
        tableView.parallaxHeader.height = parallaxHeight
        tableView.parallaxHeader.minimumHeight = 0
//        tableView.parallaxHeader.mode = .scaleToFill
        tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            print(parallaxHeader.progress)
            self.headerView.alpha = parallaxHeader.progress
            let font = self.titleLabel.font
            if parallaxHeader.progress > 0.7 {
                self.titleLabel.font  = font?.withSize(16 + parallaxHeader.progress)
                self.collectionView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRowsInSection!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResusableCell", for: indexPath)
        cell.textLabel?.text = "Row: \(indexPath.row)"
        return cell
    }
    
}

extension ViewController : UIScrollViewDelegate {
   
    //MARK: scroll view delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page: Int = {
            let pageLtR = Int(scrollView.contentOffset.x / scrollView.frame.width)
            
            if UIApplication.isRTL,
                let photosCount = images?.count {
                return photosCount - 1 - pageLtR
            }
            return pageLtR
        }()
        
        guard page != pageControl.currentPage else { return }
        pageControl.currentPage = page
    }
    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let direction:ScrollDirection = scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 ? .up : .down
        //        print(direction)
        
//        if direction == .down {
            //self.hideBar(false, theBar: .top)
           navigationController?.setNavigationBarHidden(true, animated: true)
//        }
//        } else {
//            navigationController?.setNavigationBarHidden(true, animated: true)
//        }
        
        if scrollView.isAtTop {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //        print("Stopped scrolling.....!!!!!!!!")
        //self.hideBar(false, theBar: .top)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }*/
}

//MARK:-  UIScrollView Extenssion
extension UIScrollView {
    
    var isAtTop: Bool {
        return contentOffset.y <= verticalOffsetForTop
    }
    
    var isAtBottom: Bool {
        return contentOffset.y >= verticalOffsetForBottom
    }
    
    var verticalOffsetForTop: CGFloat {
        let topInset = contentInset.top
        return -topInset
    }
    
    var verticalOffsetForBottom: CGFloat {
        let scrollViewHeight = bounds.height
        let scrollContentSizeHeight = contentSize.height
        let bottomInset = contentInset.bottom
        let scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight
        return scrollViewBottomOffset
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    // MARK:- UICollectionView DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (images?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        let width = collectionView.frame.width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        
        let imageView = cell.viewWithTag(100) as! UIImageView
        imageView.image = images?[indexPath.item]
        
        return cell
    }
}

extension UIViewController {
    enum ViewState{
        case loading, error, empty
    }
    
//    private(set) var stateView = StateView()
    
    func viewState(viewState: ViewState) {
        switch viewState {
        case .loading:
            
            break
        case .error:
            
            break
        case .empty:
            
            break
        }
    }
}
