//
//  StateView.swift
//  TestTitleView App
//
//  Created by XtraMac1 on 26/10/17.
//  Copyright © 2017 XtraMac1. All rights reserved.
//

import UIKit

protocol StateViewDataSource {
    func stateIcon() -> UIImage?
    func stateTitle() -> String?
    func stateDescription() -> String?
}


class StateView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
