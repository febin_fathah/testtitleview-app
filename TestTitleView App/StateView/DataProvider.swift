//
//  DataProvider.swift
//  TestTitleView App
//
//  Created by XtraMac1 on 26/10/17.
//  Copyright © 2017 XtraMac1. All rights reserved.
//

import Foundation
import UIKit

protocol DataProvider {
    var image: UIImage { get }
    var title: String { get }
    var description: String? { get }
    var actionName: String? { get }
}

enum DefaultProvider {
    case noData, noConnectivity, noProduct, noCategory
}


extension DefaultProvider: DataProvider {
    var image: UIImage {
        return #imageLiteral(resourceName: "noData")
    }
    
    var title: String {
        switch self {
        case .noData:
            return "No Records available"
        case .noCategory:
            return "No Categories available"
        case .noConnectivity:
            return "No Internet Connectivity"
        case .noProduct:
            return "No Products available"
        }
    }
    
    var description: String? {
        switch self {
        case .noData:
            return ""
        case .noCategory:
            return ""
        case .noConnectivity:
            return "Please check your internet connectivity"
        case .noProduct:
            return ""
        }
    }
    
    var actionName: String? {
        return nil
    }
}

struct StateManager{
    
    var stateView = StateView()
    var dataProvider: DataProvider
    
    public init(dataProfvider: DataProvider) {
        self.dataProvider = dataProfvider
    }
    
    
}
